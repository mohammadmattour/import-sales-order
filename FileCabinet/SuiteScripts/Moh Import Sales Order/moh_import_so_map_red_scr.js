/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['./utils/moh_email', './utils/moh_file', './utils/moh_search', 'N/file', 'N/log', 'N/record', 'N/search', 'N/runtime'],
/**
 * @param{file} file
 * @param{log} log
 * @param{record} record
 * @param{search} search
 */
function(emailUtil, fileUtil, searchUtil, file, log, record, search, runtime) {
   
    /**
     * Marks the beginning of the Map/Reduce process and generates input data.
     *
     * @typedef {Object} ObjectRef
     * @property {number} id - Internal ID of the record instance
     * @property {string} type - Record type id
     *
     * @return {Array|Object|Search|RecordRef} inputSummary
     * @since 2015.1
     */
    function getInputData() {
        try{

            var pendingFolder = runtime.getCurrentScript().getParameter({name:'custscript_moh_pending_folder'});

             log.debug('Folders pending', pendingFolder )
            var fileSearchObj = search.create({
                type: "file",
                filters:
                    [
                        ["folder","anyof",[pendingFolder]]
                    ],
                columns:
                    [
                        search.createColumn({
                            name: "name",
                            label: "Name"
                        }),
                        search.createColumn({name: "folder", label: "Folder"}),

                    ]
            });
            var searchResultCount = fileSearchObj.runPaged().count;
            log.debug("fileSearchObj result count",searchResultCount);

            var fileSearchResult = searchUtil.getAllResults(fileSearchObj.run());

            log.debug("fileSearchResult result count",fileSearchResult.length);

            return fileSearchResult;


        }catch (e) {
            var title = 'Error Gathering files';
            log.error({
                title: title,
                details: e
            });
            var currentUser =  runtime.getCurrentUser();
            var recipients = currentUser.id;
            emailUtil.sendEmail("Import Sales Order Error", e, -5, null, ["temp@in8sync.com"]);
        }

    }


    /**
     * Executes when the map entry point is triggered and applies to each key/value pair.
     *
     * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
     * @since 2015.1
     */


    function map(context) {
        var fileId = JSON.parse(context.value);
        var fileIdkey = JSON.parse(context.key)
        log.debug('context ', fileId);


        log.debug('Field Id ', fileId.id);


        var data = {};

        var fileObj = file.load({
            id: fileId.id
        });
        var fileContents = fileObj.getContents();

        var fileRows = fileContents.split('\r\n');
        log.debug('fileRows length ', fileRows.length);

        for (var i = 1; i< fileRows.length; i++) {

            log.debug('rowData',fileRows[i]);

            var rowData = fileRows[i].split(',');
            var orderId = rowData[0];
            var entityInternalId = rowData[1];
                var itemId = rowData[2];
                var itemQty = rowData[3];
                var itemPrice = rowData[4];
                var itemAmount = rowData[4];
            log.debug('rowData', orderId + '-' + entityInternalId + '-' +itemId + '-' +itemQty+ '-' + itemAmount);

            if (!orderId) continue;
                if (!data.hasOwnProperty(orderId))
                {
                    data[orderId] = {};
                    data[orderId] = {
                        'entityInternalId': entityInternalId,
                        'items': [{
                            'itemId': itemId,
                            'itemQty': itemQty,
                            'itemPrice': itemPrice,
                            'itemAmount': itemAmount
                        }]
                    }
                }
                else
                {
                    data[orderId].items.push({
                        'itemId': itemId,
                        'itemQty': itemQty,
                        'itemPrice': itemPrice,
                        'itemAmount': itemAmount
                    })
                }

        }
        fileUtil.moveFileToProcessedFolder(fileId.id);


        log.debug('objData', data);
        for (var orderId in data) {
            context.write({
                key: orderId,
                value: data[orderId]
            });
        }

    }

    /**
     * Executes when the reduce entry point is triggered and applies to each group.
     *
     * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
     * @since 2015.1
     */
    function reduce(context) {
        try {
            var reduceData = JSON.parse(context.values[0]);
            log.debug('reduceData', reduceData);


            if(reduceData.entityInternalId)
            {


                var objSalesOrder = record.create({
                    type: record.Type.SALES_ORDER,
                    isDynamic: true,
                })
                objSalesOrder.setValue({
                    fieldId: 'entity',
                    value: reduceData.entityInternalId
                });

                log.debug('Number Of items in the sales order', JSON.parse(reduceData.items.length));

                for (var i = 0; i < reduceData.items.length; i++) {


                    objSalesOrder.selectNewLine({ //add a line to a sublist
                        sublistId: 'item'      //specify which sublist
                    });

                    objSalesOrder.setCurrentSublistText({
                        sublistId: 'item',
                        fieldId: 'item',
                        text: reduceData.items[i].itemId
                    });

                    objSalesOrder.setCurrentSublistValue({
                        sublistId   : 'item',
                        fieldId     : 'price',
                        value       : -1
                    });

                    objSalesOrder.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'quantity',
                        value: reduceData.items[i].itemQty
                    });

                    objSalesOrder.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'rate',
                        value:  reduceData.items[i].itemPrice
                    });

                    objSalesOrder.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'amount',
                        value:  reduceData.items[i].itemAmount
                    });



                    objSalesOrder.commitLine({
                        sublistId: 'item'
                    });

                }

                var salesOrderId = objSalesOrder.save({
                    ignoreMandatoryFields: true
                });
                log.debug('Sales Order Id:', salesOrderId);
            }
        } catch (e) {
            context.write({
                key: 'Error while creating the sales order',
                value: e
            })
        }
    }


    /**
     * Executes when the summarize entry point is triggered and applies to the result set.
     *
     * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
     * @since 2015.1
     */
    function summarize(summary) {
        var allErrors = [];
        summary.mapSummary.errors.iterator().each(function(key, error) {
            allErrors.push(error);
        });
        log.debug('All Errors', allErrors);
        // The best way to send the errors is to create a file with all th lines that contains errors
        if (allErrors.length > 0) {
           emailUtil.sendEmail("Summary errors", JSON.stringify(allErrors), -5, null, ["temp@in8sync.com"]);
        }
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
    
});
