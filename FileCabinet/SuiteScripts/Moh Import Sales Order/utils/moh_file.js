define(['N/file'],
/**
 * @param{file} file
 */
function(file) {

    function moveFileToProcessedFolder(fileId, newFolderId)
    {
        try
        {


            var fileToBeMoved = file.load({
                id : fileId
            });


            fileToBeMoved.folder = -14; // save them in attachment to send file
            fileToBeMoved.save();

            file.delete({
                id: fileId
            })





        } catch (e)
        {
            log.error("error while moving file", e);

        }

    }
    return {
        moveFileToProcessedFolder: moveFileToProcessedFolder
    };
    
});
