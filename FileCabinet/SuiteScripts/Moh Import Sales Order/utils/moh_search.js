define(['N/search'],
/**
 * @param{search} search
 */
function(search) {

    function getAllResults(search) {

        var all =[];
        var results = [];

        var startIndex = 0;
        var endIndex = 1000;
        var pageSize = 1000;

        do
        {
            results = search.getRange({
                start:startIndex,
                end: endIndex
            });

            all = all.concat(results);
            log.debug('startIndex', startIndex);
            log.debug('results', results.length);

            log.debug('endIndex', endIndex);
            startIndex += pageSize;
            endIndex += pageSize;

        }while(results.length === pageSize);

        return all;

    }

    function getCustomerId(externalId)
    {
        var entitySearchObj = search.create({
            type: "entity",
            filters:
                [
                    ["externalid","anyof",externalId]
                ],
            columns:
                [
                    'internalid'
                ]
        });
        var customerId = null;
        var searchResultCount = entitySearchObj.runPaged().count;
        log.debug("entity result count",searchResultCount);
        entitySearchObj.run().each(function(result){
            customerId = result.getValue({name: "internalid"});
            return false;
        });

        return customerId;
    }
    function getItemId(itemName)
    {
        log.debug('itemName', itemName)
        var itemSearchObj = search.create({
            type: "item",
            filters:
                [
                    ["itemid","is",itemName]
                ],
            columns:
                [
                    'internalid'
                ]
        });
        var itemId = null;

        itemSearchObj.run().each(function(result){
            itemId = result.id;
            return false;
        });
        log.debug("Item internal Id",itemId);

        return itemId;
    }
    return {
        getAllResults: getAllResults,
        getCustomerId: getCustomerId,
        getItemId: getItemId
    };
    
});
