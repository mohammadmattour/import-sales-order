define(["N/email", , "N/log", "N/runtime"],

function(email, log, runtime) {

    function sendEmail(subject, body, sender, attachment, recipients){

        var senderId = sender;
        var recipients = [recipients];


        email.send({
            author: senderId,
            recipients: recipients,
            subject: subject,
            body: body,
            attachments: null
        });



    }

    return {
        sendEmail : sendEmail,
    }


});
